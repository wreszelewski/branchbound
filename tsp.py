#!/usr/bin/env python

from mpi4py import MPI
from collections import deque, defaultdict
import numpy
import sys

def find_nearest_neigh(solution, distance_table):
    node = solution['path'][-1]
    distance = None
    neigh = None
    for neigh_node in solution['avail_cities']:
        if distance is None:
            distance = distance_table[node][neigh_node]
            neigh = neigh_node
        else:
            node_dist = distance_table[node][neigh_node]
            if node_dist < distance:
                distance = distance_table[node][neigh_node]
                neigh = neigh_node
    return neigh, distance

def calculate_first_solution_greedy(distance_table):
    avail_cities = range(len(distance_table[0]))
    avail_cities.remove(0)
    solution = {'path' : [0], 'avail_cities': avail_cities, 'cost' : 0}
    while len(solution['avail_cities'])>0:
        neigh, distance = find_nearest_neigh(solution, distance_table)
        solution['path'].append(neigh)
        solution['avail_cities'].remove(neigh)
        solution['cost'] += distance 
    return solution

def calculate_first_solution_naive(distance_table):
    solution = {'path' : [0], 'avail_cities' : [], 'cost' : 0}
    for i in range(1, len(distance_table)):
        solution['path'].append(i)
        solution['cost'] += distance_table[solution['path'][-2]][solution['path'][-1]]
    return solution

def fill_status(size):
    status = []
    for i in range(1,size):
        status.append(True)
    return status

def fill_task_queue(task_queue, num_cities):
    for x in range(num_cities):
        cities = range(num_cities)
        cities.remove(x)
        task_queue.append({'path': [x], 'avail_cities' : cities, 'cost': 0})

def create_task(task, node, cost):
    path = list(task['path'])
    path.append(node)
    avail_cities = list(task['avail_cities'])
    avail_cities.remove(node)
    new_task = {
        'path' : path,
        'avail_cities' : avail_cities, 
        'cost' : cost
    }
    return new_task

def calculate_level(tasks, best, distance_table, pruning):
    temp_tasks = []
    for task in tasks:
        new_best, new_tasks, pruning = perform_task(task, best, distance_table, pruning)
        if new_best is not None:
            best = new_best
        temp_tasks.extend(new_tasks)
    
    return best, temp_tasks, pruning

def perform_task_wrapper(task, best, distance_table, pruning, levels, task_num):

    tasks = [task]
    for i in range(levels):
        task_num += len(tasks)
        new_best, tasks, pruning = calculate_level(tasks, best, distance_table, pruning)
        if new_best is not None:
            best = new_best
    return best, tasks, pruning, task_num 

def perform_task(task, best, distance_table, pruning):
    tasks = []
    new_best = None
    for node in task['avail_cities']:
        cost = task['cost'] + distance_table[task['path'][-1]][node]
        if best is not None:
            if cost < best['cost']:
                new_task = create_task(task, node, cost)
                if len(new_task['avail_cities']) == 0:
                    new_best = new_task
                    best = new_task
                tasks.append(new_task)
            else:
                pruning += 1
        else:
            new_task = create_task(task, node, cost)
            if len(new_task['avail_cities']) == 0:
                new_best = new_task
                best = new_task
            tasks.append(new_task)
    return new_best, tasks, pruning

def send_task(task_queue, source, best, status, confirm, pruning):
    data = None
    try:
        if not check_status(status):
            comm.send('End', dest=source, tag=tag)
        if best is None:
            data = task_queue.popleft()
        else:
            data = task_queue.popleft()
            while data['cost'] > best['cost']:
                data = task_queue.popleft()
                pruning += 1
    except IndexError:
        data = None
    comm.send(data, dest=source, tag=tag)
    return confirm, pruning
    

def save_best(best, source):
    data = comm.recv(source=source, tag=4)
    if best is not None:
        if data['cost'] < best['cost']:
            return data
        else:
            return best
    else:
        return data

def save_tasks(task_queue, status, source):
    data = comm.recv(source=source, tag=5)
    if data is None:
        status[source-1] = False
    else:
        status[source-1] = True
        task_queue.extend(data)

def save_stats(stats, source):
    data = comm.recv(source=source, tag=6)
    stats['workers'][source] = data
    stats['pruning'] += data['pruning']
    confirm[source-1] = False
    return stats

def check_status(status):
    return any(status)

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

filename = sys.argv[1]
start_solution = sys.argv[2]
distance_table = defaultdict(dict)

with open(filename, 'r') as input:
    for line in enumerate(input):
        for distance in enumerate(line[1].split()):
            distance_table[line[0]][distance[0]] = float(distance[1])

number_of_cities = len(distance_table[0])

if rank == 0:
    task_queue = deque()
    stats = {'pruning' : 0, 'workers' : {}}
    worker_status = fill_status(size)
    confirm = fill_status(size)
    start = MPI.Wtime()
    fill_task_queue(task_queue, number_of_cities)
    if start_solution == 'g':
        best = calculate_first_solution_greedy(distance_table)
    else:
        best = calculate_first_solution_naive(distance_table)
    print(best)
    #for i in range(1, size):
        #comm.send(task_queue.popleft(), dest=i, tag=1)
    while check_status(worker_status) or len(task_queue)>0 or check_status(confirm):
        for source in range(1, size):
            for tag in [2,3,4,5,6]:
                if comm.Iprobe(source=source, tag=tag):
                    if tag == 2:
                        comm.recv(source=source, tag=tag)
                        comm.send(best, dest=source, tag=tag)
                    elif tag == 3:
                        comm.recv(source=source, tag=tag)
                        confirm, stats['pruning'] = send_task(task_queue, source, best, worker_status, confirm, stats['pruning'])
                    elif tag == 4:
                        best = save_best(best, source)
                    elif tag == 5:
                        save_tasks(task_queue, worker_status, source)
                    elif tag == 6:
                        stats = save_stats(stats, source)
    print(best)
    print(stats) 
    stop = MPI.Wtime()
    print("Time: " + str(stop-start) + "s")
               
    #data = numpy.arange(1, dtype='i')
    #data[0] = 2
    #comm.Isend([data, MPI.INT], dest=1, tag=11)
else:
    task_number = 0
    pruning = 0
    while 1:
        best = None
        comm.send(None, dest=0, tag=2)
        best = comm.recv(source=0, tag=2)
        comm.send(None, dest=0, tag=3)
        task = comm.recv(source=0, tag=3)
        if task == 'End':
            comm.send(None, dest=0, tag=5)
            break
        if task is None:
            comm.send(None, dest=0, tag=5)
            continue
        best, tasks, pruning, task_number = perform_task_wrapper(task, best, distance_table, pruning, 3, task_number)
        if best is not None:
            comm.send(best, dest=0, tag=4)
        comm.send(tasks, dest=0, tag=5)
    worker_stats = {'tasks' : task_number, 'pruning' : pruning}
    comm.send(worker_stats, dest=0, tag=6)
    #comm.send(data, dest=0, tag=2)
    #data = numpy.empty(1, dtype='i')
    #if comm.Iprobe(source=0, tag=11):
        #comm.Irecv(data, source=0, tag=11)
        #print(data)
