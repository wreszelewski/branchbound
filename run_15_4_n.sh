#!/bin/sh
#PBS -A plgrevegrant
#PBS -l walltime=02:00:00
#PBS -l nodes=1:ppn=12
#PBS -q l_short

module load libs/boost/1.52.0
module load libs/openblas/0.2.6
pip install --user mpi4py
mpiexec -np 5 ~/branchbound/tsp.py ~/branchbound/p01_d.txt n