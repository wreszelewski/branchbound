#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include <mpi.h>


typedef struct {
    int* path;
    int cost;
    int length;
} PATH;

int** read_input_file(char* path, int number_of_cities) {

    int** distance_table = (int**) malloc(number_of_cities*sizeof(int*));
    for(int i=0; i<number_of_cities; i++) {
        distance_table[i] = (int*) malloc(number_of_cities*sizeof(int));
    }

    char *line = malloc(5000);
    size_t size=0;
    ssize_t read;

    FILE *infile = fopen(path, "r");
    if (infile == NULL) {
        printf("Error reading file: %s\n", path);
        exit(1);
    }

    int i = 0;
    while((read = getline(&line, &size, infile)) != -1) {
        for(int j=0; read > 0; j++) {
            read -= 2*sscanf(line+j*2, "%d", &distance_table[i][j]);

        }
        i++;

    }
    fclose(infile);
    free(line);

    return distance_table;
}

void free_distance_table(int** distance_table, int size) {

    for(int i; i<size; i++) {
        free(distance_table[i]);
    }
    free(distance_table);
}


int not_in(int value, int* tab, int size) {
    for(int i=0; i<size; i++) {
        if(tab[i]==value) {
            return 0;
        }
    }
    return 1;
}

int find_nearest_neighbour(int** distance_table, int* current_path, int current_node, int size, int path_marker) {
    int nearest_num=0, nearest_dist = INT_MAX;
    for(int i=0; i<size; i++) {
        if(current_node != i && distance_table[current_node][i] < nearest_dist && not_in(i, current_path, path_marker+1)) {
            nearest_dist = distance_table[current_node][i];
            nearest_num = i;
        }
    }
    return nearest_num;
}


int nearest_neigbour_solution(int** distance_table, int number_of_cities) {
   
    int* current_path = (int*) malloc(number_of_cities*(sizeof(int)));
    current_path[0] = 0;
    int current_node = 0;
    int nearest_neigh;
    int path_cost = 0;

    for(int path_marker=0; path_marker<number_of_cities; ) {
        nearest_neigh = find_nearest_neighbour(distance_table, current_path, current_node, number_of_cities, path_marker);
        path_cost += distance_table[current_node][nearest_neigh];
        current_node = nearest_neigh;
        path_marker += 1;
        current_path[path_marker] = current_node;

    }

    free(current_path);
    return path_cost;
 

}

int max_solution() {
    return INT_MAX;
}

void rewrite_path(int* path, int* old_path, int size) {
    for(int i=0; i<size; i++) {
        path[i] = old_path[i];
    }
}

PATH* calculate_task(int** distance_table, PATH task, int size) {
    PATH* paths = (PATH*) malloc(size*sizeof(PATH));
    int path_number = 0;
    for(int i=0; i<size; i++) {
        if(not_in(i, task.path, task.length)) {
            rewrite_path(paths[path_number].path, task.path, task.length);
            paths[path_number].length = task.length+1;
            paths[path_number].path[paths[path_number].length] = i;
            paths[path_number].cost = task.cost + distance_table[paths[path_number].path[paths[path_number].length-1]][i];
            path_number++;           
        } 
    }
    return paths;
}

PATH** allocate_queues(int size) {
    PATH** queues = malloc(size*sizeof(PATH*));
    for(int i=0; i<size; i++) {
        queues[i] = malloc(size*sizeof(PATH));
    }
    return queues;
}

void free_queues(PATH** queues, int size) {
    for(int i=0; i<size; i++)
        free(queues[i]);
    free(queues);
}

int main(int argc, char* argv[]) {
    if(argc<3) {
        printf("Too little arguments!\nUsage: <command> file number_of_cities [first solution]\n");
        return 1;
    }
     
    char* path = argv[1];
    int number_of_cities = atoi(argv[2]);
    char first_solution;
    if(argc==4) {
        first_solution = argv[3][0];
    } else {
        first_solution = 'm';
    }
    int** distance_table = read_input_file(path, number_of_cities);
    int result, rank, size;
    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_rank(MPI_COMM_WORLD, &size);
    
    if(rank==0) {
        printf("Master\n");
        if(first_solution=='n') {
            result = nearest_neigbour_solution(distance_table, number_of_cities);
        } else {
            result = max_solution();
        }

        printf("Path cost: %d\n", result);
    } else {
        printf("Worker\n");

    }


    MPI_Finalize();
    return 0;

}
