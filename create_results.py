import os

for name in os.listdir('.'):
    if 'run' in name:
        splitted = name.split('_')
        problem = splitted[1]
        nodes = splitted[2]
        mode = splitted[3].split('.')[0]
        with open(name, 'r') as infile:
            for _ in xrange(4):
                next(infile)
            line = next(infile)
            worker_info = eval(line)
            for worker in worker_info['workers']:
                with open("pruning_{}_{}_{}".format(problem, mode, worker), "a") as prun_file:
                    prun_file.write("{} {}\n".format(nodes, worker_info['workers'][worker]['pruning']))
                with open("tasks_{}_{}_{}".format(problem, mode, worker), "a") as tasks_file:
                    tasks_file.write("{} {}\n".format(nodes, worker_info['workers'][worker]['tasks']))
            with open("pruning_{}_{}_all".format(problem, mode), "a") as prun_file:
                prun_file.write("{} {}\n".format(nodes, worker_info['pruning']))
            line = next(infile)
            time = line.split()[1][0:-2]
            with open("time_{}_{}".format(problem, mode), "a") as time_file:
                time_file.write("{} {}\n".format(nodes, time))
