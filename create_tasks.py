import jinja2

templateLoader = jinja2.FileSystemLoader(searchpath=".")
templateEnv = jinja2.Environment(loader=templateLoader)

TEMPLATE_FILE = "template_run"

template = templateEnv.get_template(TEMPLATE_FILE)

vars = [
    {'f_name': 'sh07_dist.txt', 'nodes' : 2, 'p_nodes' : 1, 'mode' : 'n'}, 
    {'f_name': 'sh07_dist.txt', 'nodes' : 3, 'p_nodes' : 1, 'mode' : 'n'}, 
    {'f_name': 'sh07_dist.txt', 'nodes' : 5, 'p_nodes' : 1, 'mode' : 'n'}, 
    {'f_name': 'sh07_dist.txt', 'nodes' : 9, 'p_nodes' : 1, 'mode' : 'n'}, 
    {'f_name': 'sh07_dist.txt', 'nodes' : 17, 'p_nodes' : 2, 'mode' : 'n'}, 
    {'f_name': 'sh07_dist.txt', 'nodes' : 2, 'p_nodes' : 1, 'mode' : 'g'}, 
    {'f_name': 'sh07_dist.txt', 'nodes' : 3, 'p_nodes' : 1, 'mode' : 'g'}, 
    {'f_name': 'sh07_dist.txt', 'nodes' : 5, 'p_nodes' : 1, 'mode' : 'g'}, 
    {'f_name': 'sh07_dist.txt', 'nodes' : 9, 'p_nodes' : 1, 'mode' : 'g'}, 
    {'f_name': 'sh07_dist.txt', 'nodes' : 17, 'p_nodes' : 2, 'mode' : 'g'}, 
    {'f_name': 'sp11_dist.txt', 'nodes' : 2, 'p_nodes' : 1, 'mode' : 'n'}, 
    {'f_name': 'sp11_dist.txt', 'nodes' : 3, 'p_nodes' : 1, 'mode' : 'n'}, 
    {'f_name': 'sp11_dist.txt', 'nodes' : 5, 'p_nodes' : 1, 'mode' : 'n'}, 
    {'f_name': 'sp11_dist.txt', 'nodes' : 9, 'p_nodes' : 1, 'mode' : 'n'}, 
    {'f_name': 'sp11_dist.txt', 'nodes' : 17, 'p_nodes' : 2, 'mode' : 'g'}, 
    {'f_name': 'sp11_dist.txt', 'nodes' : 2, 'p_nodes' : 1, 'mode' : 'g'}, 
    {'f_name': 'sp11_dist.txt', 'nodes' : 3, 'p_nodes' : 1, 'mode' : 'g'}, 
    {'f_name': 'sp11_dist.txt', 'nodes' : 5, 'p_nodes' : 1, 'mode' : 'g'}, 
    {'f_name': 'sp11_dist.txt', 'nodes' : 9, 'p_nodes' : 1, 'mode' : 'g'}, 
    {'f_name': 'sp11_dist.txt', 'nodes' : 17, 'p_nodes' : 2, 'mode' : 'g'},
    {'f_name': 'p01_d.txt', 'nodes' : 2, 'p_nodes' : 1, 'mode' : 'n'},
    {'f_name': 'p01_d.txt', 'nodes' : 3, 'p_nodes' : 1, 'mode' : 'n'},
    {'f_name': 'p01_d.txt', 'nodes' : 5, 'p_nodes' : 1, 'mode' : 'n'},
    {'f_name': 'p01_d.txt', 'nodes' : 9, 'p_nodes' : 1, 'mode' : 'n'},
    {'f_name': 'p01_d.txt', 'nodes' : 17, 'p_nodes' : 2, 'mode' : 'n'},
    {'f_name': 'p01_d.txt', 'nodes' : 2, 'p_nodes' : 1, 'mode' : 'g'},
    {'f_name': 'p01_d.txt', 'nodes' : 3, 'p_nodes' : 1, 'mode' : 'g'},
    {'f_name': 'p01_d.txt', 'nodes' : 5, 'p_nodes' : 1, 'mode' : 'g'},
    {'f_name': 'p01_d.txt', 'nodes' : 9, 'p_nodes' : 1, 'mode' : 'g'},
    {'f_name': 'p01_d.txt', 'nodes' : 17, 'p_nodes' : 2, 'mode' : 'g'},
]

for var in vars:
    if '7' in var['f_name']:
        cities = '7'
    elif '11' in var['f_name']:
        cities = '10'
    else:
        cities = '15'
    with open('run_{}_{}_{}.sh'.format(cities, var['nodes']-1, var['mode']), 'w') as out:
        outtext = template.render(var)
        out.write(outtext)
